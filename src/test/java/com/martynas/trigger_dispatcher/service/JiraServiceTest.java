package com.martynas.trigger_dispatcher.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.martynas.trigger_dispatcher.config.JiraProps;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;


public class JiraServiceTest {

    private final RestTemplate template = new RestTemplate();
    private final MockRestServiceServer server = MockRestServiceServer.createServer(template);
    private final JiraProps props = props();
    private final JiraService service = new JiraService(template, props);

    @Test
    public void givenJiraAccountId_getJiraUserEmailTest() {
        String accountId = "0m32c93-r8qv3";
        server.expect(ExpectedCount.once(), requestTo("http://jirahost/rest/api/3/user?accountId=" + accountId))
                .andExpect(header("Authorization", "Basic dXNlcjp0b2tlbg=="))
                .andRespond(
                        withSuccess("{\"accountId\": \"0m32c93-r8qv3\", \"emailAddress\": \"test@email.com\"}",
                                MediaType.APPLICATION_JSON));

        var result = service.getUserEmail(accountId);
        assertEquals("test@email.com", result);
        server.verify();
    }

    private JiraProps props() {
        var props = new JiraProps();
        props.setUsername("user");
        props.setToken("token");
        props.setUrl("http://jirahost/");
        return props;
    }
}
