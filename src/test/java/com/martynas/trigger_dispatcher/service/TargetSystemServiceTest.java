package com.martynas.trigger_dispatcher.service;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.martynas.trigger_dispatcher.config.TargetSystemProps;
import com.martynas.trigger_dispatcher.config.JiraProps;
import com.martynas.trigger_dispatcher.entity.jira.Issue;
import com.martynas.trigger_dispatcher.entity.jira.IssueData;
import com.martynas.trigger_dispatcher.entity.jira.IssueType;
import com.martynas.trigger_dispatcher.entity.jira.User;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

public class TargetSystemServiceTest {

    private final RestTemplate template = new RestTemplate();
    private final MockRestServiceServer server = MockRestServiceServer.createServer(template);
    private final TargetSystemProps targetSystemProps = targetSystemProps();
    private final JiraProps jiraProps = jiraProps();
    private final JiraService jiraService = new JiraService(template, jiraProps);
    private final TargetSystemService targetSystemService = new TargetSystemService(template, targetSystemProps, jiraProps,
            jiraService);


    @Test
    public void givenReporterIsNotCustomer_sendCustomerEmailToTargetSystemTest() {

        Issue issueContent = new Issue("00001", "PROD-0001",
                new IssueData(
                        new User("007", "assignee", "assignee@email.com"),
                        new User(jiraProps.getDummyAccountId(), "reporter", "reporter@email.com"),
                        "customer@email.com",
                        new IssueType("333", "Incident"),
                        "test issue summary"));

        server.expect(ExpectedCount.once(), requestTo("http://target_endpoint:8000/trigger"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().string("{\"apiKey\":\"0aJd-me231-pur231\","
                        + "\"targetId\":\"PROD-0001\","
                        + "\"event\":\"targetEvent\","
                        + "\"parameters\":{"
                        + "\"issueKey\":\"PROD-0001\","
                        + "\"recipientEmail\":\"customer@email.com\","
                        + "\"issueType\":\"Incident\","
                        + "\"summary\":\"test issue summary\"}}"))
                .andRespond(
                        withSuccess());

        targetSystemService.sendTrigger(issueContent);
        server.verify();
    }

    private TargetSystemProps targetSystemProps() {
        var props = new TargetSystemProps();
        props.setApiKey("0aJd-me231-pur231");
        props.setUrl("http://target_endpoint:8000/");
        props.setTriggerEventName("targetEvent");
        return props;
    }

    private JiraProps jiraProps() {
        var props = new JiraProps();
        props.setToken("token");
        props.setUsername("user");
        props.setUrl("http://jirahost");
        props.setDummyAccountId("account_id");
        return props;
    }
}