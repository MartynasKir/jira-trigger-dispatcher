package com.martynas.trigger_dispatcher.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.martynas.trigger_dispatcher.entity.jira.Issue;
import com.martynas.trigger_dispatcher.service.TargetSystemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TriggerEventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TargetSystemService service;

    @Test
    public void whenNoJsonGiven_feedbackEventControllerReturnBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/trigger-dispatcher/api/v1/trigger")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenBadJson_feedbackEventControllerReturnBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/trigger-dispatcher/api/v1/trigger")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"bad\":\"json\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenCorrectJson_feedbackEventControllerReturnStatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/trigger-dispatcher/api/v1/trigger")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"issue\":{"
                        + "\"id\": \"123\","
                        + "\"key\": \"1245\","
                        + "\"fields\":{"
                        + "\"assignee\":{\"accountId\":\"123\",\"name\": \"assigneeName\"},"
                        + "\"reporter\":{\"accountId\":\"312\",\"name\": \"reporterName\"},"
                        + "\"customfield_007\":\"customer@email.com\","
                        + "\"issuetype\":{\"id\":\"432\",\"name\": \"PROD-0001\"},"
                        + "\"summary\":\"test summary\"}}}"))
                .andExpect(status().isOk());

        verify(service).sendTrigger(any(Issue.class));
    }
}
