package com.martynas.trigger_dispatcher.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Component
@Validated
@ConfigurationProperties("app.jira")
public class JiraProps {

    @NotBlank
    private String url;

    @NotBlank
    private String username;

    @NotBlank
    private String token;

    @NotBlank
    private String dummyAccountId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDummyAccountId() {
        return dummyAccountId;
    }

    public void setDummyAccountId(String dummyAccountId) {
        this.dummyAccountId = dummyAccountId;
    }
}
