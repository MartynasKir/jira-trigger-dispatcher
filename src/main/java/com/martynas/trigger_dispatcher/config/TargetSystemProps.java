package com.martynas.trigger_dispatcher.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Component
@Validated
@ConfigurationProperties(prefix = "app.target-system")
public class TargetSystemProps {

    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9-:./]+")
    private String url;

    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9-]+")
    private String apiKey;

    @NotBlank
    private String triggerEventName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTriggerEventName() {
        return triggerEventName;
    }

    public void setTriggerEventName(String triggerEventName) {
        this.triggerEventName = triggerEventName;
    }

}
