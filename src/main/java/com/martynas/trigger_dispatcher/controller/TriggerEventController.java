package com.martynas.trigger_dispatcher.controller;

import com.martynas.trigger_dispatcher.entity.jira.JiraWebhookContent;
import com.martynas.trigger_dispatcher.service.TargetSystemService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class TriggerEventController {

    private final TargetSystemService targetService;

    public TriggerEventController(TargetSystemService targetService) {
        this.targetService = targetService;
    }

    @PostMapping(value = "/trigger-dispatcher/api/v1/trigger", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void forwardWebhookData(@Valid @RequestBody JiraWebhookContent jiraWebhookContent) {
        targetService.sendTrigger(jiraWebhookContent.getIssueContent());
    }
}
