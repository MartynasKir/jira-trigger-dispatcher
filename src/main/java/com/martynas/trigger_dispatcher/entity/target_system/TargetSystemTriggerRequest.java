package com.martynas.trigger_dispatcher.entity.target_system;

public class TargetSystemTriggerRequest {

    private final String apiKey;
    private final String targetId;
    private final String event;
    private final Parameters parameters;

    public TargetSystemTriggerRequest(String apiKey,
            String targetId,
            String event,
            Parameters parameters) {
        this.apiKey = apiKey;
        this.targetId = targetId;
        this.event = event;
        this.parameters = parameters;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getEvent() {
        return event;
    }

    public Parameters getParameters() {
        return parameters;
    }
}
