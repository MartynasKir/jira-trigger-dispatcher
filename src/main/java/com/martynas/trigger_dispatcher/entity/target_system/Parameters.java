package com.martynas.trigger_dispatcher.entity.target_system;

public class Parameters {

    private final String issueKey;
    private final String recipientEmail;
    private final String issueType;
    private final String summary;

    public Parameters(String issueKey,
            String recipientEmail,
            String issueType,
            String summary) {
        this.issueKey = issueKey;
        this.recipientEmail = recipientEmail;
        this.issueType = issueType;
        this.summary = summary;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public String getIssueType() {
        return issueType;
    }

    public String getSummary() {
        return summary;
    }
}
