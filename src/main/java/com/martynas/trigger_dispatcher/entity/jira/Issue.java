package com.martynas.trigger_dispatcher.entity.jira;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Issue {

    @NotBlank
    private final String issueId;

    @NotBlank
    private final String issueKey;

    @NotNull
    @Valid
    private final IssueData issueData;

    @JsonCreator
    public Issue(@JsonProperty("id") String issueId,
            @JsonProperty("key") String issueKey,
            @JsonProperty("fields") IssueData issueData) {
        this.issueId = issueId;
        this.issueKey = issueKey;
        this.issueData = issueData;
    }

    public String getIssueId() {
        return issueId;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public IssueData getIssueData() {
        return issueData;
    }
}

