package com.martynas.trigger_dispatcher.entity.jira;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class JiraWebhookContent {

    @NotNull
    @Valid
    private final Issue issueContent;

    @JsonCreator
    public JiraWebhookContent(@JsonProperty("issue") Issue issueContent) {
        this.issueContent = issueContent;
    }

    public Issue getIssueContent() {
        return issueContent;
    }

}
