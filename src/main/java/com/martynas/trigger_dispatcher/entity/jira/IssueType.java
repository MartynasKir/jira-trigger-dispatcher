package com.martynas.trigger_dispatcher.entity.jira;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;

public class IssueType {

    @NotBlank
    private final String issueId;

    @NotBlank
    private final String issueName;

    @JsonCreator
    public IssueType(@JsonProperty("id") String issueId,
            @JsonProperty("name") String issueName) {
        this.issueId = issueId;
        this.issueName = issueName;
    }

    public String getIssueId() {
        return issueId;
    }

    public String getIssueName() {
        return issueName;
    }
}
