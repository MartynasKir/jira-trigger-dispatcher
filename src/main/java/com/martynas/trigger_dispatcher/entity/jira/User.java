package com.martynas.trigger_dispatcher.entity.jira;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class User {

    @NotBlank
    private final String accountId;

    @NotBlank
    private final String name;

    private final String emailAddress;

    @JsonCreator
    public User(@JsonProperty("accountId") String accountId,
            @JsonProperty("name") String name,
            @JsonProperty("emailAddress") String emailAddress) {
        this.accountId = accountId;
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
