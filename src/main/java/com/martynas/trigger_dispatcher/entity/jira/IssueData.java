package com.martynas.trigger_dispatcher.entity.jira;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class IssueData {

    @Valid
    @NotNull
    private final User assignee;

    @Valid
    @NotNull
    private final User reporter;

    private final String customerEmail;

    @Valid
    @NotNull
    private final IssueType issueType;

    @NotBlank
    private final String summary;

    @JsonCreator
    public IssueData(@JsonProperty("assignee") User assignee,
            @JsonProperty("reporter") User reporter,
            @JsonProperty("customfield_007") String customerEmail,
            @JsonProperty("issuetype") IssueType issueType,
            @JsonProperty("summary") String summary) {
        this.assignee = assignee;
        this.reporter = reporter;
        this.customerEmail = customerEmail;
        this.issueType = issueType;
        this.summary = summary;
    }

    public User getAssignee() {
        return assignee;
    }

    public User getReporter() {
        return reporter;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public String getSummary() {
        return summary;
    }
}
