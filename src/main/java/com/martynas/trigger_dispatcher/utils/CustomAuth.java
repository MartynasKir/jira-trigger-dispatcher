package com.martynas.trigger_dispatcher.utils;

import org.apache.tomcat.util.codec.binary.Base64;

import java.nio.charset.Charset;

public final class CustomAuth {

    private CustomAuth() {
    }

    public static String returnAuthHeader(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.defaultCharset()));
        return "Basic " + new String(encodedAuth);
    }
}
