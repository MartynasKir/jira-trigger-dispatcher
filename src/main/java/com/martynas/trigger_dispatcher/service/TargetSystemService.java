package com.martynas.trigger_dispatcher.service;

import static org.slf4j.LoggerFactory.getLogger;

import com.martynas.trigger_dispatcher.config.TargetSystemProps;
import com.martynas.trigger_dispatcher.config.JiraProps;
import com.martynas.trigger_dispatcher.entity.target_system.TargetSystemTriggerRequest;
import com.martynas.trigger_dispatcher.entity.target_system.Parameters;
import com.martynas.trigger_dispatcher.entity.jira.Issue;
import org.slf4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TargetSystemService {

    private static final Logger LOG = getLogger(TargetSystemService.class);
    private static final String TRIGGER_ENDPOINT = "trigger";

    private final RestTemplate restTemplate;
    private final TargetSystemProps targetSystemProps;
    private final JiraProps jiraProps;
    private final String targetSystemEndpoint;
    private final HttpHeaders headers;
    private final JiraService jiraService;

    public TargetSystemService(RestTemplate restTemplate,
            TargetSystemProps targetSystemProps,
            JiraProps jiraProps,
            JiraService jiraService) {
        this.restTemplate = restTemplate;
        this.jiraService = jiraService;
        this.targetSystemProps = targetSystemProps;
        this.jiraProps = jiraProps;
        this.targetSystemEndpoint = targetSystemProps.getUrl() + TRIGGER_ENDPOINT;
        this.headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public void sendTrigger(Issue issueContent) {
        String recipientEmail = getRecipientEmail(issueContent);

        Parameters requestParameters = new Parameters(
                issueContent.getIssueKey(),
                recipientEmail,
                issueContent.getIssueData().getIssueType().getIssueName(),
                issueContent.getIssueData().getSummary()
        );

        TargetSystemTriggerRequest targetSystemTriggerRequest = new TargetSystemTriggerRequest(
                targetSystemProps.getApiKey(),
                issueContent.getIssueKey(),
                targetSystemProps.getTriggerEventName(),
                requestParameters
        );

        HttpEntity<TargetSystemTriggerRequest> requestEntity = new HttpEntity<>(targetSystemTriggerRequest, headers);
        restTemplate.postForLocation(targetSystemEndpoint, requestEntity);
        LOG.info("Event for ticket id: '{}' is sent to target system",
                requestEntity.getBody().getParameters().getIssueKey());

    }

    private String getRecipientEmail(Issue issueContent) {

        String accountId = issueContent.getIssueData().getReporter().getAccountId();
        String recipientEmail;

        if (accountId.equals(jiraProps.getDummyAccountId())) {
            LOG.info("Reporter of: '{}' is external customer", issueContent.getIssueKey());
            recipientEmail = issueContent.getIssueData().getCustomerEmail();
            LOG.info("Got recipient email from Jira Customer Email field: '{}'", recipientEmail);
            if (recipientEmail == null) {
                LOG.info("Custom field: 'Customer Email' is empty for ticket: '{}'", issueContent.getIssueKey());
            }

        } else {
            LOG.info("Reporter of '{}' is internal colleague", issueContent.getIssueKey());
            recipientEmail = jiraService.getUserEmail(accountId);
            LOG.info("Got recipient email from Jira reporter field: {}", recipientEmail);
            if (recipientEmail == null) {
                //LOG message indicates Jira API GDPR constraint
                LOG.info("Failure to get reporter email. User: '{}' email might not be public",
                        issueContent.getIssueData().getReporter().getName());
            }
        }
        return recipientEmail;
    }
}
