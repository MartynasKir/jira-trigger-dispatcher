package com.martynas.trigger_dispatcher.service;

import com.martynas.trigger_dispatcher.config.JiraProps;
import com.martynas.trigger_dispatcher.entity.jira.User;
import com.martynas.trigger_dispatcher.utils.CustomAuth;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class JiraService {

    private static final String GET_USER_ENDPOINT = "rest/api/3/user?accountId=";
    private final RestTemplate restTemplate;
    private final JiraProps jiraProps;
    private final HttpHeaders headers;

    public JiraService(RestTemplate restTemplate, JiraProps jiraProps) {
        this.restTemplate = restTemplate;
        this.jiraProps = jiraProps;
        this.headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.AUTHORIZATION,
                CustomAuth.returnAuthHeader(jiraProps.getUsername(), jiraProps.getToken()));
    }

    public String getUserEmail(String accountId) {
        String callUrl = jiraProps.getUrl() + GET_USER_ENDPOINT + accountId;

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<User> response = restTemplate.exchange(callUrl, HttpMethod.GET, entity, User.class);

        return response.getBody().getEmailAddress();
    }
}
