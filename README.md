This dispatcher project purpose is to receive particular webhooks from Jira system about the closed tickets(condition
when webhook is sent configured in Jira), then based on whether reporter is internal user or external customer, build
json request to target_system with recipient information which will later on send the email (html template) to particular
user.

dispatcher distinguishes external user from internal user by dummyAccountId which is shown as reporter when ticket
came outside of an organisation. If reporter is dummyAccount, then we know that ticket is created by customer and its
email value is taken from custom field other than reporter field in jira.

For example this solution used to send a trigger to other system which send email to external customer to:
  request for feedback about the resolved incident.
  send other ticket related updates.



